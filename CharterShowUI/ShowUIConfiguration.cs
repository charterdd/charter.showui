﻿using Rocket.API;

namespace Charter.ShowUI
{
    public class ShowUIConfiguration : IRocketPluginConfiguration
    {
        public bool ShowOnJoinUI;
        public ushort ShowOnJoinID;
        public bool ShowOnReviveUI;
        public ushort ShowOnReviveID;
        public void LoadDefaults()
        {
            ShowOnJoinUI = true;
            ShowOnReviveID = 7290;
            ShowOnReviveUI = true;
            ShowOnReviveID = 7290;
        }
    }
}
