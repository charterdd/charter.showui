﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System.Collections.Generic;

namespace Charter.ShowUI
{
    public class CommandClose : IRocketCommand
    {
        public List<string> Aliases
        {
            get
            {
                return new List<string> { };
            }
        }

        public AllowedCaller AllowedCaller
        {
            get
            {
                return AllowedCaller.Player;
            }
        }

        public string Help
        {
            get
            {
                return "Close opened UI";
            }
        }

        public string Name
        {
            get
            {
                return "close";
            }
        }

        public List<string> Permissions
        {
            get
            {
                return new List<string> { "charter.command.close" };
            }
        }

        public string Syntax
        {
            get
            {
                return "";
            }
        }
        public void Execute(IRocketPlayer caller, string[] command)
        {
            UnturnedPlayer player = (UnturnedPlayer)caller;
            EffectManager.askEffectClearByID(ShowUI.Instance.Configuration.Instance.ShowOnReviveID, player.CSteamID);
            EffectManager.askEffectClearByID(ShowUI.Instance.Configuration.Instance.ShowOnJoinID, player.CSteamID);
        }
    }
}
