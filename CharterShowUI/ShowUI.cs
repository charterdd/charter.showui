﻿using Rocket.Core.Plugins;
using Rocket.Unturned;
using Rocket.Unturned.Events;
using Rocket.Unturned.Player;
using SDG.Unturned;
using System;
using System.Collections;
using System.IO;
using System.Net;
using UnityEngine;
using Logger = Rocket.Core.Logging.Logger;

namespace Charter.ShowUI
{
    public class ShowUI : RocketPlugin<ShowUIConfiguration>
    {
        public static ShowUI Instance;
        protected override void Load()
        {
            Logger.Log("CharterShowUI has been loaded!");
            Instance = this;
            U.Events.OnPlayerConnected += Events_OnPlayerConnected;
            UnturnedPlayerEvents.OnPlayerRevive += OnRevive;
        }
        protected override void Unload()
        {
            Rocket.Core.Logging.Logger.Log("CharterShowUI has been unloaded!");
            U.Events.OnPlayerConnected -= Events_OnPlayerConnected;
            UnturnedPlayerEvents.OnPlayerRevive -= OnRevive;
            StopAllCoroutines();
        }
        private void Events_OnPlayerConnected(UnturnedPlayer player)
        {
            if (Configuration.Instance.ShowOnJoinUI == true)
            {
                player.TriggerEffect(Configuration.Instance.ShowOnJoinID);
            }
        }

        IEnumerator OnPlayerRevive(UnturnedPlayer player)
        {
            yield return new WaitForSeconds(.1f);
            player.TriggerEffect(Configuration.Instance.ShowOnReviveID);
        }

        void OnRevive(UnturnedPlayer player, Vector3 position, byte angle)
        {
            StartCoroutine(OnPlayerRevive(player));
        }
    }
}
